<!-- JavaScript Variables -->
# 1- Biến là gì?
    Trong quá trình xây dựng website hoặc các ứng dụng với Javascript chúng ta sẽ cần phải làm việc với các dạng thông tin dữ liệu khác nhau. Quan sát 3 ví dụ sau:
    -   Phần mềm kế toán - Chúng ta sẽ làm việc với những con số
    -   Website bán hàng - Làm việc với dữ liệu thông tin sản phẩm, đơn hàng và giỏ hàng
    -   Ứng dụng Chat - Dữ liệu là những đoạn chat, tin nhắn, thông tin người chat

    => Biến được sử dụng để lưu trữ các thông tin trên trong quá trình ứng dụng Javascript hoạt động.


# 2- Khai báo biến
    -   Để khai báo biến ta sẽ bắt đầu bằng từ khóa var (var là viết tắt của từ variable - nghĩa là biến). 
    Khai báo biến có cú pháp như sau:

        var [dấu cách] [tên biến];
    ví dụ:
        var variablesName;
    Biến trên được tạo ra với [variableName] là tên biến và chúng ta có thể lưu giá trị cho biến [variableName] bằng cách:
        variableName = 'javascritp cơ bản: Biến trong javascritp';
    -   Chúng ta có thể vừa khai báo và gán giá trị cho biến ngay sau khi khai báo
    -   Ngoài cách khai báo thông thường chúng ta có thể khai báo nhiều biến cùng lúc và gán giá trị cho biến như sau:
        
        var fullName = 'Nguyễn Diên Sỹ Đạo',
            age = 22,
            job = 'Developer';

## Bài tập
1>
-   Tạo biến bossName và gán giá trị cho biến này là tên thú cưng của bạn
-   Tạo biến bossAge và gán cho giá trị là số tuổi thú cưng của bạn
-   Tạo biến senName để lưu tên của bạn
-   Tạo biến senAge để lưu số tuổi của bạn
-   Hiển thị giá trị biến bossName với hàm alert.
2>
-   Tạo biến currentCourse và gán cho giá trị là 'Javascript cơ bản'
-   Tạo biến newCourse và sao chép giá trị của biến currentCourse
-   Sử dụng hàm alert hiển thị giá trị của biến currentCourse
-   Sử dụng hàm alert hiển thị giá trị của biến newCourse
-   Sửa giá trị của biến newCourse thành 'Javascript nâng cao'
-   Sử dụng hàm alert hiển thị giá trị của biến newCourse sau khi sửa