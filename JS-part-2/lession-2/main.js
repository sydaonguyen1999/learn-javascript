// BÀI TẬP

// -   Tạo biến bossName và gán giá trị cho biến này là tên thú cưng của bạn
// -   Tạo biến bossAge và gán cho giá trị là số tuổi thú cưng của bạn
// -   Tạo biến senName để lưu tên của bạn
// -   Tạo biến senAge để lưu số tuổi của bạn
// -   Hiển thị giá trị biến bossName với hàm alert.


var bossName = 'Tên thú cưng',
    bossAge = 1,
    senName = 'Nguyễn Diên Sỹ Đạo',
    senAge = 22;
alert(bossName)

// -   Tạo biến currentCourse và gán cho giá trị là 'Javascript cơ bản'
// -   Tạo biến newCourse và sao chép giá trị của biến currentCourse
// -   Sử dụng hàm alert hiển thị giá trị của biến currentCourse
// -   Sử dụng hàm alert hiển thị giá trị của biến newCourse
// -   Sửa giá trị của biến newCourse thành 'Javascript nâng cao'
// -   Sử dụng hàm alert hiển thị giá trị của biến newCourse sau khi sửa

var currentCourse = 'Javascript cơ bản';
var newCourse = 'Javascript nâng cao';
console.log('currentCourse',currentCourse);
console.log('newCourse',newCourse);