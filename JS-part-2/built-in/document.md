<!-- JavaScript built-in function -->

<!-- link: https://fullstack.edu.vn/learning/javascript-co-ban/mot-so-ham-built-in-trong-js -->
# 1- built-in function là gì?
    Giới thiệu một số built-in function
        1. Alert
        2. Console
        3. Confirm
        4. Prompt
        5. Set timeout
        6. Set interval

--- 1. Alert
-   Hàm alert dùng để in thông báo ra màn hình, trong hàm alert chúng ta có thể đưa vào trong hàm này biến, string hoặc có thể là 1 số.
    cú pháp:
        alert('')
--- 2. Console
-   Với đối tượng Console chúng ta sẽ có 1 số hàm cơ bản như sau:
        console.warn(warn);
    
        console.log(log);
        console.error(error);
-   Với những hàm của đối tượng Console chúng ta có thể in ra những giá trị của biến hoặc chuỗi hoặc có thể là bất cứ những giá trị gì.
-   Đối tượng Console thường được sử dụng để debug, giúp chúng ta in ra các giá trị cần biết trong lúc code.

--- 3. Confirm
-   Hàm này được dùng để hiện thông báo lên website với message và button ok hoặc hủy

--- 4. Prompt
-   Tương tự như hàm Confirm nhưng ở Prompt chúng ta có thêm ô input để nhập giá trị và có thể lấy giá trị đó.

--- 5. Set timeout
-   Hàm Set timeout truyền vào 1 function và sẻ thực thi function này sau thời gian chúng ta set.
    Cú pháp:

        setTimeout(function(){
                confirm('Xác nhận lại thông tin')
            }, 2000)

--- 5. Set Interval
-   Hàm này có cú pháp viết và cách tương tự như setTimeout nhưng chỉ khác là với setTimeout thì function được truyền vào sẻ thực thi sau thời gian và dừng lại còn đối với setInterval thì function được truyền vào sẻ thực thi sau thời gian set và không dừng lại.




### BÀI TẬP
Bài tập JavaScript 1: Cho người dùng nhập vào tên và tuổi. Hãy thông báo tên và tuổi của người đó ra màn hình console, Và xác nhận câu đúng sai bằng hàm Confirm sau 1 giây.